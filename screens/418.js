import React, { useEffect, useRef } from "react";
import { View, Text, StyleSheet, Animated, Easing, Image } from "react-native";

function TeapotScreen() {
    const spinValue = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.loop(
        Animated.timing(spinValue, {
            toValue: 1,
            duration: 2000,
            easing: Easing.linear,
            useNativeDriver: true,
        })
        ).start();
    }, [spinValue]);

    const spin = spinValue.interpolate({
        inputRange: [0, 1],
        outputRange: ["0deg", "360deg"],
    });

    return (
        <View style={styles.container}>
        <View style={styles.errorContainer}>
            <Text style={styles.errorText}>ERROR</Text>
            <Animated.Text style={[styles.errorText, { transform: [{ rotate: spin }] }]}>418</Animated.Text>
        </View>
        <Animated.Image
            style={[styles.teapot, { transform: [{ rotate: spin }] }]}
            source={{ uri: "https://s3.amazonaws.com/images.seroundtable.com/http-418-teapot-1408710394.png" }}
        />
        <Text style={styles.message}>
            Any attempt to brew coffee with a teapot should result in the error code 418 I'm a teapot. The resulting entity
            body MAY be short and stout.
        </Text>
        <View style={styles.errorContainer}>
            <Animated.Text style={[styles.errorText, { transform: [{ rotate: spin }] }]}>418</Animated.Text>
            <Text style={styles.errorText}>ERROR</Text>
        </View>
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 20,
    },
    teapot: {
        width: 200,
        height: 200,
    },
    message: {
        fontSize: 24,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 20,
        marginBottom: 40,
    },
    errorContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginBottom: 20,
    },
    errorText: {
        fontSize: 48,
        fontWeight: "bold",
        color: "#ff0000",
        marginHorizontal: 10,
    },
});

export default TeapotScreen;
