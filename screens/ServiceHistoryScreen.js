import React, { useState, useEffect } from "react";
import { View, Text, TextInput, TouchableOpacity, FlatList, StyleSheet } from "react-native";

function ServiceHistoryScreen() {
    const [appointments, setAppointments] = useState([]);
    const [searchVIN, setSearchVIN] = useState("");
    const [filteredAppointments, setFilteredAppointments] = useState([]);

    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
        setFilteredAppointments(data.appointments);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = () => {
        if (searchVIN) {
        setFilteredAppointments(
            appointments.filter((appointment) =>
            appointment.vin.toLowerCase().includes(searchVIN.toLowerCase())
            )
        );
        } else {
        setFilteredAppointments(appointments);
        }
    };

    const handleInputChange = (value) => {
        setSearchVIN(value);
    };

    const formatDate = (dateString) => {
        const options = {
        month: "2-digit",
        day: "2-digit",
        year: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric",
        hour12: true,
        };
        return new Date(dateString).toLocaleString("en-US", options);
    };

    const renderItem = ({ item }) => {
        return (
        <View style={styles.row}>
            <Text style={[styles.cell, styles.vin]}>{item.vin}</Text>
            <Text style={[styles.cell, styles.customer]}>{item.customer}</Text>
            <Text style={[styles.cell, styles.dateTime]}>{formatDate(item.date_time)}</Text>
            <Text style={[styles.cell, styles.technician]}>
            {item.technician.first_name} {item.technician.last_name}
            </Text>
            <Text style={[styles.cell, styles.reason]}>{item.reason}</Text>
            <View style={styles.status}>
            <Text style={[styles.statusText, item.status === "Completed" ? styles.success : styles.warning]}>
                {item.status}
            </Text>
            </View>
        </View>
        );
    };

    return (
        <View style={styles.container}>
        <Text style={styles.heading}>Service History</Text>
        <View style={styles.searchContainer}>
            <Text style={styles.label}>Search by VIN</Text>
            <View style={styles.inputContainer}>
            <TextInput
                style={styles.input}
                value={searchVIN}
                onChangeText={handleInputChange}
                placeholder="Enter VIN"
            />
            <TouchableOpacity style={styles.searchButton} onPress={handleSearch}>
                <Text style={styles.searchButtonText}>Search</Text>
            </TouchableOpacity>
            </View>
        </View>
        <FlatList
            data={filteredAppointments}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            style={styles.list}
        />
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#1E1E1E",
        paddingTop: 20,
        paddingHorizontal: 10,
    },
    heading: {
        fontSize: 24,
        fontWeight: "bold",
        color: "#fff",
        marginBottom: 20,
    },
    searchContainer: {
        marginBottom: 20,
    },
    label: {
        color: "#fff",
        fontSize: 16,
        marginBottom: 10,
    },
    inputContainer: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#fff",
        borderRadius: 5,
        padding: 10,
    },
    input: {
        flex: 1,
        fontSize: 16,
        color: "#000",
    },
    searchButton: {
        backgroundColor: "#FF4136",
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginLeft: 10,
    },
    searchButtonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 16,
    },
    list: {
        width: "100%",
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#4D4D4D",
    },
    cell: {
        flex: 1,
        color: "#fff",
        textAlign: "center",
        fontSize: 16,
    },
    vin: {
        flex: 2,
    },
    customer: {
        flex: 3,
    },
    dateTime: {
        flex: 4,
    },
    technician: {
        flex: 3,
    },
    reason: {
        flex: 3,
    },
    status: {
        flex: 2,
        borderRadius: 5,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    statusText: {
        fontWeight: "bold",
        fontSize: 16,
        textAlign: "center",
    },
    success: {
        backgroundColor: "#28A745",
        color: "#fff",
    },
    warning: {
        backgroundColor: "#FFC107",
        color: "#000",
    },
});

export default ServiceHistoryScreen;
