import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { useSpring, animated } from "react-spring/native";
import { View, Text, TextInput, TouchableOpacity, Image } from "react-native";

const LoginForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [funFact, setFunFact] = useState("");
    const [selectedAPI, setSelectedAPI] = useState("");
    const [dogImage, setDogImage] = useState("");
    const fadeIn = useSpring({ opacity: 1, from: { opacity: 0 } });

    const navigation = useNavigation();

    const handleSubmit = async () => {
        try {
        const response = await axios.post(
            "http://localhost:8000/token",
            `grant_type=password&username=${email}&password=${password}&scope=&client_id=&client_secret=`,
            {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                Accept: "application/json",
            },
            }
        );
        localStorage.setItem("token", response.data.access_token);
        console.log("Logged in successfully");
        alert(`User ${email} was logged in successfully`);
        // Navigate to home page and refresh
        navigation.navigate("/");
        resetForm();
        } catch (error) {
        console.error(error.response.data);
        setErrorMessage(
            "Incorrect email or password. Are you sure you're a human?"
        );
        }
    };

    const resetForm = () => {
        setEmail("");
        setPassword("");
        setErrorMessage("");
    };

    const handleShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleAPISelection = async (api) => {
        setSelectedAPI(api);
    };

    useEffect(() => {
        const fetchFunFact = async () => {
        try {
            let url = "";
            let headers = {};
            switch (selectedAPI) {
            case "Chuck Norris Jokes":
                url = "https://api.chucknorris.io/jokes/random";
                break;
            case "Dad Jokes":
                url = "https://icanhazdadjoke.com/";
                headers = { Accept: "application/json" };
                break;
            case "Cat Facts":
                url = "https://catfact.ninja/fact";
                break;
            case "Dog Images":
                url = "https://dog.ceo/api/breeds/image/random";
                const response = await axios.get(url);
                setDogImage(response.data.message);
                break;
            case "Advice Slip":
                url = "https://api.adviceslip.com/advice";
                break;
            case "Jokes":
                url = "https://sv443.net/jokeapi/v2/joke/Any";
                break;
            default:
                url = "";
                break;
            }
            if (url) {
            const response = await axios.get(url, { headers });
            setFunFact(
                response.data.message ||
                response.data.value ||
                response.data.joke ||
                response.data.fact ||
                response.data.slip?.advice ||
                (response.data.setup && response.data.delivery
                    ? response.data.setup + " " + response.data.delivery
                    : undefined)
            );
            } else {
            setFunFact("");
            }
        } catch (error) {
            console.error(error);
            setFunFact("Failed to fetch fun fact :(");
        }
        };

        fetchFunFact();
    }, [selectedAPI]);

    return (
        <View
        style={{
            flex: 1,
            backgroundColor: "#F87171",
            justifyContent: "center",
            alignItems: "center",
            padding: 10,
        }}
        >
        <animated.View
            style={[
            {
                backgroundColor: "#fff",
                borderRadius: 10,
                shadowColor: "#000",
                shadowOffset: {
                width: 0,
                height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
                padding: 10,
                maxWidth: "90%",
            },
            fadeIn,
            ]}
        >
            <View style={{ marginBottom: 8 }}>
            <Text style={{ fontWeight: "bold", fontSize: 20 }}>Email:</Text>
            <TextInput
                style={{
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                fontSize: 20,
                marginTop: 5,
                }}
                value={email}
                onChangeText={setEmail}
                keyboardType="email-address"
                autoCapitalize="none"
                autoCompleteType="email"
            />
            </View>
            <View style={{ marginBottom: 8 }}>
            <Text style={{ fontWeight: "bold", fontSize: 20 }}>Password:</Text>
            <View
                style={{
                flexDirection: "row",
                alignItems: "center",
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginTop: 5,
                }}
            >
                <TextInput
                style={{ flex: 1, fontSize: 20 }}
                secureTextEntry={!showPassword}
                value={password}
                onChangeText={setPassword}
                autoCapitalize="none"
                autoCompleteType="password"
                />
                <TouchableOpacity onPress={handleShowPassword}>
                <Text
                    style={{
                    color: "#000",
                    fontWeight: "bold",
                    marginLeft: 10,
                    fontSize: 20,
                    }}
                >
                    {showPassword ? "Hide" : "Show"}
                </Text>
                </TouchableOpacity>
            </View>
            </View>
            {errorMessage ? (
            <Text style={{ color: "red", marginBottom: 8, fontSize: 20 }}>
                {errorMessage}
            </Text>
            ) : null}
            <TouchableOpacity
            onPress={handleSubmit}
            style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                alignItems: "center",
                marginTop: 10,
            }}
            >
            <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Login
            </Text>
            </TouchableOpacity>
        </animated.View>
        <View style={{ alignItems: "center", marginTop: 20 }}>
            <Text style={{ fontWeight: "bold", fontSize: 20 }}>
            Select an API to generate a fun fact:
            </Text>
            <View style={{ flexDirection: "row", marginTop: 10 }}>
            <TouchableOpacity
                onPress={() => handleAPISelection("Chuck Norris Jokes")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Chuck Norris Jokes
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => handleAPISelection("Dad Jokes")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Dad Jokes
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => handleAPISelection("Cat Facts")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Cat Facts
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => handleAPISelection("Dog Images")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Dog Images
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => handleAPISelection("Advice Slip")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Advice Slip
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={() => handleAPISelection("Jokes")}
                style={{
                backgroundColor: "#3B82F6",
                padding: 10,
                borderRadius: 5,
                marginRight: 5,
                }}
            >
                <Text style={{ color: "#fff", fontWeight: "bold", fontSize: 20 }}>
                Jokes
                </Text>
            </TouchableOpacity>
            </View>
            <View style={{ marginTop: 10 }}>
            {funFact ? (
                <Text style={{ fontSize: 20 }}>{funFact}</Text>
            ) : (
                <Text style={{ fontSize: 20 }}>
                No fun fact generated yet. Select an API to generate one.
                </Text>
            )}
            </View>
            {dogImage && (
            <View style={{ marginTop: 10 }}>
                <Image
                source={{ uri: dogImage }}
                style={{ width: 200, height: 200, borderRadius: 10 }}
                />
            </View>
            )}
        </View>
        </View>
    );
};

export default LoginForm;
