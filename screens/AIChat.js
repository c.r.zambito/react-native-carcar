import React, { useState } from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import axios from "axios";

export default function AIChat() {
  const [messages, setMessages] = useState([
    {
      message: "Hiya! Let's come up with some ideas together!",
      type: "thinkbot",
    },
  ]);
  const [input, setInput] = useState("");

  const sendMessage = async () => {
    try {
      const res = await axios.post("http://localhost:8000/chat", {
        message: input,
      });
      const newMessages = [
        ...messages,
        { message: input, type: "guest" },
        { message: res.data.answer, type: "thinkbot" },
      ];
      setMessages(newMessages);
      setInput("");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.chatBox}>
        {messages.map((message, index) => (
          <View style={styles.message} key={index}>
            <Text style={styles.sender}>
              {message.type === "thinkbot" ? "ThinkBot" : "Guest"}
            </Text>
            <Text style={styles.messageText}>{message.message}</Text>
          </View>
        ))}
      </View>
      <View style={styles.inputBox}>
        <TextInput
          style={styles.input}
          value={input}
          onChangeText={(text) => setInput(text)}
          placeholder="Type your message here"
        />
        <Button title="Send" onPress={sendMessage} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  chatBox: {
    flex: 1,
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
  },
  message: {
    marginBottom: 10,
  },
  sender: {
    fontWeight: "bold",
  },
  messageText: {
    marginLeft: 10,
  },
  inputBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    flex: 1,
    borderColor: "#ddd",
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginRight: 10,
  },
});
