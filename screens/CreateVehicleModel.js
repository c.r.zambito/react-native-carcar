import React, { useState, useEffect } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";
import { Picker } from '@react-native-picker/picker';

function CreateVehicleModel() {
    const [manufacturers, setManufacturers] = useState([]);
    const [formData, setFormData] = useState({
        name: "",
        picture_url: "",
        manufacturer_id: "",
    });

    const handleFormChange = (inputName, value) => {
        setFormData({
        ...formData,
        [inputName]: value,
        });
    };

    const handleSubmit = async () => {
        const automobileModelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
            "Content-Type": "application/json",
        },
        };

        const response = await fetch(automobileModelUrl, fetchConfig);
        if (response.ok) {
        setFormData({
            name: "",
            picture_url: "",
            manufacturer_id: "",
        });
        }
    };

    const fetchManufacturers = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);
        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
        }
    };

    useEffect(() => {
        fetchManufacturers();
    }, []);

    return (
        <View style={styles.container}>
        <Text style={styles.heading}>New Model</Text>
        <View style={styles.form}>
            <View style={styles.inputContainer}>
            <TextInput
                style={styles.input}
                onChangeText={(value) => handleFormChange("name", value)}
                value={formData.name}
                placeholder="Model Name"
                required
            />
            <Text style={styles.label}>Model Name</Text>
            </View>
            <View style={styles.inputContainer}>
            <TextInput
                style={styles.input}
                onChangeText={(value) => handleFormChange("picture_url", value)}
                value={formData.picture_url}
                placeholder="Picture URL"
                required
            />
            <Text style={styles.label}>Picture URL</Text>
            </View>
            <View style={styles.inputContainer}>
            <Text style={styles.label}>Choose a Manufacturer</Text>
            <Picker
                selectedValue={formData.manufacturer_id}
                style={styles.picker}
                onValueChange={(itemValue) =>
                handleFormChange("manufacturer_id", itemValue)
                }
            >
                <Picker.Item label="Select a Manufacturer" value="" />
                {manufacturers.map((manufacturer) => (
                <Picker.Item
                    key={manufacturer.id}
                    label={manufacturer.name}
                    value={manufacturer.id}
                />
                ))}
            </Picker>
            </View>
            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
            <Text style={styles.buttonText}>Create!</Text>
            </TouchableOpacity>
        </View>
        </View>
    );
    }

    const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#1E1E1E",
        paddingTop: 20,
        paddingHorizontal: 10,
    },
    heading: {
        fontSize: 24,
        fontWeight: "bold",
        color: "#fff",
        marginBottom: 20,
        textAlign: "center",
    },
    form: {
        backgroundColor: "#f8f9fa",
        borderRadius: 10,
        padding: 20,
    },
    inputContainer: {
        marginBottom: 20,
    },
    input: {
        backgroundColor: "#fff",
        borderRadius: 5,
        padding: 10,
        fontSize: 16,
        color: "#000",
    },
    label: {
        color: "#000",
        fontSize: 16,
        marginBottom: 5,
    },
    picker: {
        backgroundColor: "#fff",
        borderRadius: 5,
        padding: 10,
        fontSize: 16,
        color: "#000",
    },
    button: {
        backgroundColor: "#ff416c",
        borderRadius: 5,
        paddingVertical: 10,
        paddingHorizontal: 20,
        alignSelf: "center",
        marginTop: 20,
    },
    buttonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 16,
        textAlign: "center",
    },
});

export default CreateVehicleModel;

// import React, { useState, useEffect } from "react";
// import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

// function CreateVehicleModel() {
//     const [manufacturers, setManufacturers] = useState([]);
//     const [formData, setFormData] = useState({
//         name: "",
//         picture_url: "",
//         manufacturer_id: "",
//     });

//     const handleFormChange = (inputName, value) => {
//         setFormData({
//         ...formData,
//         [inputName]: value,
//         });
//     };

//     const handleSubmit = async () => {
//         const automobileModelUrl = "http://localhost:8100/api/models/";
//         const fetchConfig = {
//         method: "POST",
//         body: JSON.stringify(formData),
//         headers: {
//             "Content-Type": "application.json",
//         },
//         };

//         const response = await fetch(automobileModelUrl, fetchConfig);
//         if (response.ok) {
//         setFormData({
//             name: "",
//             picture_url: "",
//             manufacturer_id: "",
//         });
//         }
//     };

//     const fetchManufacturers = async () => {
//         const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
//         const response = await fetch(manufacturerUrl);
//         if (response.ok) {
//         const data = await response.json();
//         setManufacturers(data.manufacturers);
//         }
//     };

//     useEffect(() => {
//         fetchManufacturers();
//     }, []);

//     return (
//         <View style={styles.container}>
//         <Text style={styles.heading}>New Model</Text>
//         <View style={styles.form}>
//             <View style={styles.inputContainer}>
//             <TextInput
//                 style={styles.input}
//                 onChangeText={(value) => handleFormChange("name", value)}
//                 value={formData.name}
//                 placeholder="Model Name"
//                 required
//             />
//             <Text style={styles.label}>Model Name</Text>
//             </View>
//             <View style={styles.inputContainer}>
//             <TextInput
//                 style={styles.input}
//                 onChangeText={(value) => handleFormChange("picture_url", value)}
//                 value={formData.picture_url}
//                 placeholder="Picture URL"
//                 required
//             />
//             <Text style={styles.label}>Picture URL</Text>
//             </View>
//             <View style={styles.inputContainer}>
//             <Text style={styles.label}>Choose a Manufacturer</Text>
//             <View style={styles.dropdown}>
//                 {manufacturers.map((manufacturer) => {
//                 return (
//                     <TouchableOpacity
//                     key={manufacturer.id}
//                     style={styles.dropdownItem}
//                     onPress={() =>
//                         handleFormChange("manufacturer_id", manufacturer.id)
//                     }
//                     >
//                     <Text style={styles.dropdownText}>{manufacturer.name}</Text>
//                     </TouchableOpacity>
//                 );
//                 })}
//             </View>
//             </View>
//             <TouchableOpacity style={styles.button} onPress={handleSubmit}>
//             <Text style={styles.buttonText}>Create!</Text>
//             </TouchableOpacity>
//         </View>
//         </View>
//     );
//     }

//     const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: "#1E1E1E",
//         paddingTop: 20,
//         paddingHorizontal: 10,
//     },
//     heading: {
//         fontSize: 24,
//         fontWeight: "bold",
//         color: "#fff",
//         marginBottom: 20,
//         textAlign: "center",
//     },
//     form: {
//         backgroundColor: "#f8f9fa",
//         borderRadius: 10,
//         padding: 20,
//     },
//     inputContainer: {
//         marginBottom: 20,
//     },
//     input: {
//         backgroundColor: "#fff",
//         borderRadius: 5,
//         padding: 10,
//         fontSize: 16,
//         color: "#000",
//     },
//     label: {
//         color: "#fff",
//         fontSize: 16,
//         marginBottom: 5,
//     },
//     dropdown: {
//         backgroundColor: "#fff",
//         borderRadius: 5,
//         padding: 10,
//         fontSize: 16,
//         color: "#000",
//     },
//     dropdownItem: {
//         paddingVertical: 10,
//         paddingHorizontal: 5,
//     },
//     dropdownText: {
//         fontSize: 16,
//         color: "#000",
//     },
//     button: {
//         backgroundColor: "linear-gradient(to right, #ff416c, #ff4b2b)",
//         borderRadius: 5,
//         paddingVertical: 10,
//         paddingHorizontal: 20,
//         alignSelf: "center",
//         marginTop: 20,
//     },
//     buttonText: {
//         color: "#fff",
//         fontWeight: "bold",
//         fontSize: 16,
//         textAlign: "center",
//     },
// });

// export default CreateVehicleModel;
