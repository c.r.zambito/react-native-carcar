import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./screens/HomeScreen";
import ListScreen from "./screens/ListScreen";
import Navbar from "./Navbar";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          header: ({ scene }) => (
            <Navbar
              title={
                scene && scene.descriptor ? scene.descriptor.options.title : ""
              }
            />
          ),
        }}
      >
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "Home" }}
        />
        <Stack.Screen
          name="List"
          component={ListScreen}
          options={{ title: "List" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


// import { NavigationContainer } from "@react-navigation/native";
// import { createStackNavigator } from "@react-navigation/stack";
// import NavigationBar from "react-native-navbar";
// import { View, StatusBar } from "react-native";

// import HomeScreen from "./screens/HomeScreen";
// import ListAutomobileScreen from "./screens/ListAutomobileScreen";
// import ScheduleServiceScreen from "./screens/ScheduleServiceScreen";
// import CarScreen from "./screens/CarScreen";
// import AddTechnicianScreen from "./screens/AddTechnicianScreen";
// import TechnicianListScreen from "./screens/TechnicianListScreen";

// const Stack = createStackNavigator();

// const styles = {
//   container: {
//     flex: 1,
//     paddingTop: StatusBar.currentHeight + 60, // Add padding to the top (10 units)
//     backgroundColor: "black", // Change the color to black
//   },
// };

// const rightButtonConfig = {
//   title: "Next",
//   handler: () => alert("hello!"),
// };

// const titleConfig = {
//   title: "Hello, world",
// };

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <NavigationBar
//         title={titleConfig}
//         rightButton={rightButtonConfig}
//         tintColor="red" // Change the color to black
//       />
//       <NavigationContainer>
//         <Stack.Navigator headerMode="none">
//           <Stack.Screen name="Home" component={HomeScreen} />
//           <Stack.Screen name="Automobiles" component={ListAutomobileScreen} />
//           <Stack.Screen
//             name="Schedule Service"
//             component={ScheduleServiceScreen}
//           />
//           <Stack.Screen name="Car List" component={CarScreen} />
//           <Stack.Screen name="Add Technician" component={AddTechnicianScreen} />
//           <Stack.Screen
//             name="Technician List"
//             component={TechnicianListScreen}
//           />
//         </Stack.Navigator>
//       </NavigationContainer>
//     </View>
//   );
// }

// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';
// import NavigationBar from 'react-native-navbar';

// import HomeScreen from './screens/HomeScreen';
// import ListAutomobileScreen from './screens/ListAutomobileScreen';
// import ScheduleServiceScreen from './screens/ScheduleServiceScreen';
// import CarScreen from './screens/CarScreen';
// import AddTechnicianScreen from './screens/AddTechnicianScreen';
// import TechnicianListScreen from './screens/TechnicianListScreen';

// const Stack = createStackNavigator();

// export default function App() {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator>
//         <Stack.Screen name="Home" component={HomeScreen} />
//         <Stack.Screen name="Automobiles" component={ListAutomobileScreen} />
//         <Stack.Screen name="Schedule Service" component={ScheduleServiceScreen} />
//         <Stack.Screen name="Car List" component={CarScreen} />
//         <Stack.Screen name="Add Technician" component={AddTechnicianScreen} />
//         <Stack.Screen name="Technician List" component={TechnicianListScreen} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }
