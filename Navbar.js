import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function Navbar({ title }) {
    const navigation = useNavigation();

    return (
        <View style={{ height: 80, backgroundColor: "lightblue", padding: 20 }}>
        <TouchableOpacity onPress={() => navigation.navigate("Home")}>
            <Text style={{ fontSize: 20 }}>{title}</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
            <TouchableOpacity
            style={{ marginRight: 20 }}
            onPress={() => navigation.navigate("List")}
            >
            <Text>List</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Settings")}>
            <Text>Settings</Text>
            </TouchableOpacity>
        </View>
        </View>
    );
}
